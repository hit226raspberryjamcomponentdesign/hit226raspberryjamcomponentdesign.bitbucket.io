/*** Navigation Toggle***/ 

const toggleButton = document.getElementsByClassName('toggleButton')[0];
toggleButton.addEventListener('click', navToggle);

//Function to show/hide the navigation links for mobile devices
function navToggle(){
    const navLinks = document.getElementsByClassName('navLinks')[0];
    navLinks.classList.toggle('mobileNavLinks');
}

/*** Generate meal based on option selected***/ 

const budgetButton = document.getElementById('budgetButton');
budgetButton.addEventListener('click', showBudgetMeal);

//Function to show the budgetMeal option when selecting the Budget button
function showBudgetMeal(){
    document.getElementById('placeholderMeal').className = "mealContainer yourMealHide";
    document.getElementById('budgetMeal').className = "mealContainer";
    document.getElementById('toplineMeal').className = "mealContainer yourMealHide";
    document.getElementById('surpriseMeal').className = "mealContainer yourMealHide";
    document.getElementById('yourMeal').scrollIntoView({behavior: "smooth"});
}

const toplineButton = document.getElementById('toplineButton');
toplineButton.addEventListener('click', showToplineMeal);

//Function to show the toplineMeal option when selecting the Top of the Line button
function showToplineMeal(){
    document.getElementById('placeholderMeal').className = "mealContainer yourMealHide";
    document.getElementById('budgetMeal').className = "mealContainer yourMealHide";
    document.getElementById('toplineMeal').className = "mealContainer";
    document.getElementById('surpriseMeal').className = "mealContainer yourMealHide";
    document.getElementById('yourMeal').scrollIntoView({behavior: "smooth"});
}

const surpriseButton = document.getElementById('surpriseButton');
surpriseButton.addEventListener('click', showSurpriseMeal);

//Function to show the surpriseMeal option when selecting the Surprise button
function showSurpriseMeal(){
    document.getElementById('placeholderMeal').className = "mealContainer yourMealHide";
    document.getElementById('budgetMeal').className = "mealContainer yourMealHide";
    document.getElementById('toplineMeal').className = "mealContainer yourMealHide";
    document.getElementById('surpriseMeal').className = "mealContainer";
    document.getElementById('yourMeal').scrollIntoView({behavior: "smooth"});
}



